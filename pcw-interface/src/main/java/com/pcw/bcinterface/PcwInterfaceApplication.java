package com.pcw.bcinterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
public class PcwInterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcwInterfaceApplication.class, args);
	}

}
