package com.pcw.bcinterface.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.pcw.bcinterface.entity.AvailableTechRole;
import com.pcw.bcinterface.entity.Pcw;
import com.pcw.bcinterface.entity.PcwCertificate;
import com.pcw.bcinterface.entity.PcwEducation;
import com.pcw.bcinterface.entity.PcwProject;
import com.pcw.bcinterface.entity.PcwResume;
import com.pcw.bcinterface.entity.PcwSocialMedia;
import com.pcw.bcinterface.entity.Role;
import com.pcw.bcinterface.entity.Tech;
import com.pcw.bcinterface.entity.TechLevel;
import com.pcw.bcinterface.model.TechLevelSheetModel;
import com.pcw.bcinterface.model.utils.TemplateAppender;
import com.pcw.bcinterface.model.utils.ExcelUtils;
import com.pcw.bcinterface.pdfgenerator.Generator;
import com.pcw.bcinterface.pdfgenerator.PdfGeneratorBuilder;
import com.pcw.bcinterface.repository.AvailableTechRoleRepository;
import com.pcw.bcinterface.repository.PcwRepository;
import com.pcw.bcinterface.repository.ResumeRepository;
import com.pcw.bcinterface.repository.RoleRepository;
import com.pcw.bcinterface.repository.TechRepository;

@Service
@Transactional
public class FileServiceImpl implements FileService {

	@Autowired
	PcwRepository pcwRepository;
	
	@Autowired
	ResumeRepository resumeRepository;

	@Autowired
	TechRepository techRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	private PdfGeneratorBuilder pdfGeneratorBuilder;

	@Autowired
	AvailableTechRoleRepository availableTechRoleRepository;

	@Override
	public void saveResumeFilePath(Long pcwId, MultipartFile file) {

		StringBuilder fileName = new StringBuilder();
		String uploadDirectory = "FilesFolder";
		Path fileNameAndPath = Paths.get(uploadDirectory, "Resume"+pcwId+UUID.randomUUID().toString()+"."+FilenameUtils.getExtension(file.getOriginalFilename()));
		fileName.append(file.getOriginalFilename() + " ");
		try {
			if (!Files.exists(Paths.get(uploadDirectory))) {
				Files.createDirectory(Paths.get(uploadDirectory));
			}
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		PcwResume resume = new PcwResume();
		Pcw pcw=pcwRepository.getOne(pcwId);
		resume.setPcw(pcw);
		resume.setFilePath(fileNameAndPath.toString());
		resumeRepository.save(resume);
	}

	@Override
	public void storeTechLevelSheet(MultipartFile file) {

		try {
			Workbook workbook = new XSSFWorkbook(file.getInputStream());
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rows = sheet.iterator();
			List<AvailableTechRole> availableTechRoleList = new ArrayList<AvailableTechRole>();

			int rowNumber = 0;
			while (rows.hasNext()) {
				Row currentRow = rows.next();

				// skip header
				if (rowNumber == 0) {
					rowNumber++;
					continue;
				}

				Iterator<Cell> cellsInRow = currentRow.iterator();
				int cellIdx = 0;

				Tech tech = null;
				Role role = null;
				AvailableTechRole availableTechRole = null;

				while (cellsInRow.hasNext()) {
					Cell currentCell = cellsInRow.next();

					switch (cellIdx) {
					case 0:
						tech = techRepository.findByName(currentCell.getStringCellValue()).get(0);
						break;
					case 1:
						role = roleRepository.findByName(currentCell.getStringCellValue()).get(0);
						break;

					case 2:
						setAvailbleTechRoleValues(availableTechRoleList, tech, role, new AvailableTechRole(),
								currentCell, new TechLevel(1L));
						break;
					case 3:
						setAvailbleTechRoleValues(availableTechRoleList, tech, role, new AvailableTechRole(),
								currentCell, new TechLevel(2L));
						break;
					case 4:
						setAvailbleTechRoleValues(availableTechRoleList, tech, role, new AvailableTechRole(),
								currentCell, new TechLevel(3L));
						break;
					case 5:
						setAvailbleTechRoleValues(availableTechRoleList, tech, role, new AvailableTechRole(),
								currentCell, new TechLevel(4L));
						break;
					case 6:
						setAvailbleTechRoleValues(availableTechRoleList, tech, role, new AvailableTechRole(),
								currentCell, new TechLevel(5L));
						break;
					default:
						break;
					}
					cellIdx++;
				}
			}
			workbook.close();
			availableTechRoleRepository.saveAll(availableTechRoleList);
		} catch (IOException e) {
			throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		}
	}

	private void setAvailbleTechRoleValues(List<AvailableTechRole> availableTechRoleList, Tech tech, Role role,
			AvailableTechRole availableTechRole, Cell currentCell, TechLevel techLevel) {
		availableTechRole.setRole(role);
		availableTechRole.setTech(tech);
		availableTechRole.setTechLevel(techLevel);
		availableTechRole.setCount((int) currentCell.getNumericCellValue());
		availableTechRoleList.add(availableTechRole);
	}

	@Override
	public byte[] downloadTechLevelSheet() {
		List<TechLevelSheetModel> techLevelSheetList = new ArrayList<>();
		List<AvailableTechRole> availableTechRoleList = availableTechRoleRepository.findAll();

		Map<String, List<AvailableTechRole>> techBasedMap = availableTechRoleList.stream()
				.collect(Collectors.groupingBy(a -> a.getTech().getName()));

		for (Entry<String, List<AvailableTechRole>> techEntry : techBasedMap.entrySet()) {

			Map<String, List<AvailableTechRole>> roleBasedMap = techEntry.getValue().stream()
					.collect(Collectors.groupingBy(a -> a.getRole().getName()));

			for (Entry<String, List<AvailableTechRole>> roleEntry : roleBasedMap.entrySet()) {

				TechLevelSheetModel techLevelSheetModel = new TechLevelSheetModel();
				techLevelSheetModel.setRole(roleEntry.getKey());
				techLevelSheetModel.setTechnology(techEntry.getKey());
				
				for (AvailableTechRole aTechRole : roleEntry.getValue()) {

					switch (aTechRole.getTechLevel().getId().intValue()) {
					case 1:
						techLevelSheetModel.setCountLevel1(aTechRole.getCount());
						break;
					case 2:
						techLevelSheetModel.setCountLevel2(aTechRole.getCount());
						break;
					case 3:
						techLevelSheetModel.setCountLevel3(aTechRole.getCount());
						break;
					case 4:
						techLevelSheetModel.setCountLevel4(aTechRole.getCount());
						break;
					case 5:
						techLevelSheetModel.setCountLevel5(aTechRole.getCount());
						break;
					default:
						break;
					}
				}
				techLevelSheetList.add(techLevelSheetModel);
			}
		}

		byte[] outStream = new ExcelUtils().writeToExcelInMultiSheets("Report", techLevelSheetList);
		return outStream == null ? new ByteArrayOutputStream().toByteArray() : outStream;
	}

	@Override
	public byte[] getResume(Long pcwId) throws Exception {
		
		String htmlTemplateString = TemplateAppender.read("resume-template.html");
		String finaleString = "";
		Map<String, String> params = new HashMap<>();

		Pcw pcw = pcwRepository.getOne(pcwId);
		params.put("empName", pcw.getUser().getName() == null ? "" : pcw.getUser().getName());
		params.put("empEmail", pcw.getUser().getEmail() == null ? "" : pcw.getUser().getEmail());
		params.put("empMobile", pcw.getUser().getPhone() == null ? "" : pcw.getUser().getPhone());
		params.put("empProjects", getEmpProjects(pcw.getPcwProjects()));
		params.put("empEducation", getEmpEducations(pcw.getPcwEducations()));
		params.put("empCertificates", getEmpCertificates(pcw.getPcwCertificates()));
		params.put("empSocialLinks", getEmpSocialLinks(pcw.getPcwSocialMedias()));
		finaleString = TemplateAppender.parseTemplateFromParams(htmlTemplateString, params);
		
		Generator generator = pdfGeneratorBuilder.create().withXhtml(finaleString)
				.build();
		Map<String, Object> data = new HashMap<>();
		byte[] pdfBuffer = generator.toBytes(data);
		return pdfBuffer;
	}

	private String getEmpSocialLinks(List<PcwSocialMedia> pcwSocialMedias) {
		StringBuffer sb = new StringBuffer();
		sb.append("<section>\r\n" + 
				"			<div class=\"sectionTitle\">\r\n" + 
				"				<h1>Social Media</h1>\r\n" + 
				"			</div>\r\n" + 
				"			\r\n" + 
				"			<div class=\"sectionContent\">\r\n" + 
				"				<ul class=\"keySkills\">\r\n");
				
				for(PcwSocialMedia pcSocMed:pcwSocialMedias) {
					sb.append("<li>"+pcSocMed.getType() +" : "+pcSocMed.getLink()+"</li>\r\n");
				}
				sb.append("				</ul>\r\n" + 
				"			</div>\r\n" + 
				"			<div class=\"clear\"></div>\r\n" + 
				"		</section>");
		return sb.toString();
	}

	private String getEmpCertificates(List<PcwCertificate> pcwCertificates) {
		StringBuffer sb = new StringBuffer();
		sb.append("<section>\r\n" + 
				"			<div class=\"sectionTitle\">\r\n" + 
				"				<h1>Key Skills</h1>\r\n" + 
				"			</div>\r\n" + 
				"			\r\n" + 
				"			<div class=\"sectionContent\">\r\n" + 
				"				<ul class=\"keySkills\">\r\n");
				
				for(PcwCertificate pcCert:pcwCertificates) {
					sb.append("<li>"+pcCert.getTitle()+"</li>\r\n");
					sb.append("<p class=\"subDetails\">"+pcCert.getYear()+"</p>\r\n"); 
				}
				sb.append("				</ul>\r\n" + 
				"			</div>\r\n" + 
				"			<div class=\"clear\"></div>\r\n" + 
				"		</section>");
		return sb.toString();
	}

	private String getEmpEducations(List<PcwEducation> list) {
		StringBuffer sb = new StringBuffer();
		sb.append("<section><div class=\"sectionTitle\"><h1>Education </h1></div>");
				for(PcwEducation pcEdu:list) {
					sb.append(
							"			<div class=\"sectionContent\">\r\n" + 
							"				<article>\r\n" + 
							"					<h2>"
							+ pcEdu.getDegreeName()
							+ "</h2>\r\n" + 
							"					<p class=\"subDetails\">"+pcEdu.getYear()+"</p>\r\n" + 
							"					<p>"
							+ "Completed "+pcEdu.getDegreeName()+" from "+pcEdu.getUniversityName()+" in year of "+pcEdu.getYear()
							+ "</p>\r\n" + 
							"				</article>\r\n" +
							"			</div>\r\n");
				}
				sb.append("			<div class=\"clear\"></div>\r\n" + 
				"		</section>");
		return sb.toString();
	}

	private String getEmpProjects(List<PcwProject> pcwProjects) {
		StringBuffer sb = new StringBuffer();
		sb.append("<section><div class=\"sectionTitle\"><h1>Project </h1></div>");
				for(PcwProject pcProj:pcwProjects) {
					sb.append(
							"			<div class=\"sectionContent\">\r\n" + 
							"				<article>\r\n" + 
							"					<h2>"
							+ pcProj.getTitle()
							+ "</h2>\r\n" + 
							"					<p>"
							+ pcProj.getDescription()
							+ "</p>\r\n" + 
							"				</article>\r\n" +
							"			</div>\r\n");
				}
				sb.append("			<div class=\"clear\"></div>\r\n" + 
				"		</section>");
		return sb.toString();
	}

}