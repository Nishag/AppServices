package com.pcw.bcinterface.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pcw.bcinterface.entity.UserDevice;
import com.pcw.bcinterface.model.DeviceInfo;
import com.pcw.bcinterface.repository.UserDeviceRepository;

@Service
public class UserDeviceServiceImpl implements UserDeviceService {

	@Autowired
	private UserDeviceRepository userDeviceRepository;

	/**
	 * Creates a new user device and set the user to the current device
	 */
	public UserDevice createUserDevice(DeviceInfo deviceInfo) {
		UserDevice userDevice = new UserDevice();
		userDevice.setDeviceId(deviceInfo.getDeviceId());
		userDevice.setDeviceType(deviceInfo.getDeviceType());
		userDevice.setNotificationToken(deviceInfo.getNotificationToken());
		userDevice.setIsRefreshActive(true);
		return userDevice;
	}

	@Override
	public Optional<UserDevice> findByUserId(Long id) {
		return userDeviceRepository.findByUserId(id);
	}

}