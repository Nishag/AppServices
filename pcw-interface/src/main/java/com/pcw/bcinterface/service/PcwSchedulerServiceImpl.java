package com.pcw.bcinterface.service;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pcw.bcinterface.entity.Pcw;
import com.pcw.bcinterface.entity.PcwResume;
import com.pcw.bcinterface.entity.User;
import com.pcw.bcinterface.repository.PcwRepository;

@Service
@Transactional
public class PcwSchedulerServiceImpl implements PcwSchedulerService {

	@Autowired
	private PcwRepository pcwRepository;

	@Override
	public void deletePcw() {
		try {
			List<Pcw> pcwList = pcwRepository.findAll();
			for (Pcw pcw : pcwList) {
				if (new Date().after(pcw.getDeleteOn()) && !pcw.getIsDeleted()) {
					System.out.println("YES DATE AFTER");
					User user = pcw.getUser();
					user.setCompany(null);
					user.setEmail(null);
					user.setIsdCode(null);
					user.setName(null);
					user.setPhone(null);
					pcw.getPcwCertificates().clear();
					pcw.getPcwEducations().clear();
					pcw.getPcwProjects().clear();
					pcw.getPcwSocialMedias().clear();
					pcw.setIsDeleted(true);
					
					for(PcwResume resume:pcw.getResume()) {
						Files.deleteIfExists(Paths.get(resume.getFilePath()));
					}
					pcw.getResume().clear();
				}
				pcwRepository.save(pcw);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}