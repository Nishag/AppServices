package com.pcw.bcinterface.service;

import com.pcw.bcinterface.model.RefreshToken;

public interface RefreshTokenService {

	public void deleteById(Long id);

	public RefreshToken createRefreshToken();

	public RefreshToken save(RefreshToken refreshToken);
}
