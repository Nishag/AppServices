package com.pcw.bcinterface.service;


import java.util.List;

import com.pcw.bcinterface.model.ApplyJobModel;
import com.pcw.bcinterface.model.JobDetailsModel;
import com.pcw.bcinterface.model.JobSearchModel;
import com.pcw.bcinterface.model.JobsModel;
import com.pcw.bcinterface.model.PaymentModel;

public interface JobService {

	List<JobsModel> getJobList(Long id, List<String> jobStatus, Integer paginationLimit, Integer pageNumber);
	JobDetailsModel getJobDetailList(Long pcw_id, Long job_id);
	List<JobSearchModel> getSearchJob(Long pcw_id, Long techid, Long roleid, Integer page_size, Integer page_number);
	void applyJob(ApplyJobModel jobModel);
	void acceptJobRequest(ApplyJobModel jobModel);
	void rejectJobRequest(ApplyJobModel jobModel);
	void addPaymentDetails(PaymentModel paymentModel);

}
