package com.pcw.bcinterface.service;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.security.core.Authentication;

import com.pcw.bcinterface.entity.User;
import com.pcw.bcinterface.model.LoginRequest;
import com.pcw.bcinterface.model.RefreshToken;


public interface AuthService {

	Optional<Authentication> authenticateUser(LoginRequest loginRequest);

	Optional<RefreshToken> createAndPersistRefreshTokenForDevice(Authentication authentication,
			@Valid LoginRequest loginRequest, User user);
}
