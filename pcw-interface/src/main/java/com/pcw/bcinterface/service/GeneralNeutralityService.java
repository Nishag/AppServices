package com.pcw.bcinterface.service;

import java.util.Map;

public interface GeneralNeutralityService {

	public Map<Object, Object> isGenderNeutrality(String text, String pcwFullName);
}
