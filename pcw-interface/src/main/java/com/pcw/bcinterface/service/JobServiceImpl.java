package com.pcw.bcinterface.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.pcw.bcinterface.entity.Job;
import com.pcw.bcinterface.entity.JobPcw;
import com.pcw.bcinterface.entity.Pcw;
import com.pcw.bcinterface.entity.PcwBankDetails;
import com.pcw.bcinterface.exception.CustomException;
import com.pcw.bcinterface.model.ApplyJobModel;
import com.pcw.bcinterface.model.JobDetailsModel;
import com.pcw.bcinterface.model.JobSearchModel;
import com.pcw.bcinterface.model.JobsModel;
import com.pcw.bcinterface.model.PaymentModel;
import com.pcw.bcinterface.model.utils.StringConstants;
import com.pcw.bcinterface.repository.JobPcwRepository;
import com.pcw.bcinterface.repository.JobRepository;
import com.pcw.bcinterface.repository.JobTechRoleRepository;
import com.pcw.bcinterface.repository.PaymentRepository;

@Service
@Transactional
public class JobServiceImpl implements JobService {

	@Autowired
	JobPcwRepository jobpcw;

	@Autowired
	JobRepository jobRepo;

	@Autowired
	JobTechRoleRepository jobTecRole;

	@Autowired
	PaymentRepository paymentRepo;

	@Override
	public List<JobsModel> getJobList(Long id, List<String> jobStatus, Integer pageSize, Integer pageNumber) {
		return jobpcw.getJobList(id, jobStatus, PageRequest.of(pageNumber - 1, pageSize));
	}

	@Override
	public JobDetailsModel getJobDetailList(Long pcw_id, Long job_id) {
		return jobpcw.getJobDetailList(pcw_id, job_id);
	}

	@Override
	public List<JobSearchModel> getSearchJob(Long pcwId, Long techId, Long roleId, Integer pageSize,
			Integer pageNumber) {
		return jobTecRole.getSearchJob(pcwId, techId, roleId, PageRequest.of(pageNumber - 1, pageSize));
	}

	@Override
	public void applyJob(ApplyJobModel jobModel) {

		JobPcw jobPcw = jobpcw.getJobPcwByJobIdAndPcwId(jobModel.getJobId(), jobModel.getPcwId());
		if (jobPcw == null) {
			jobPcw = new JobPcw();
			jobPcw.setJob(new Job(jobModel.getJobId()));
			jobPcw.setPcw(new Pcw(jobModel.getPcwId()));
			jobPcw.setStatus(StringConstants.PENDING);
			jobpcw.save(jobPcw);
		} else {
			throw new CustomException(
					"The Person " + jobModel.getPcwId() + " Already Applied For this Job " + jobModel.getJobId(),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public void acceptJobRequest(ApplyJobModel jobModel) {
		JobPcw jobPcw = jobpcw.getJobPcwByJobIdAndPcwId(jobModel.getJobId(), jobModel.getPcwId());
		jobPcw.setStatus(StringConstants.requestAccepted);
		jobpcw.save(jobPcw);
	}

	@Override
	public void rejectJobRequest(ApplyJobModel jobModel) {
		JobPcw jobPcw = jobpcw.getJobPcwByJobIdAndPcwId(jobModel.getJobId(), jobModel.getPcwId());
		jobPcw.setStatus(StringConstants.requestRejected);
		jobpcw.save(jobPcw);
	}

	@Override
	public void addPaymentDetails(PaymentModel paymentModel) {
		PcwBankDetails bDetails = new PcwBankDetails();
		bDetails.setPcw(new Pcw(paymentModel.getPcwId()));
		bDetails.setBankName(paymentModel.getBankName());
		bDetails.setIfscCode(paymentModel.getIFSCCode());
		paymentRepo.save(bDetails);
	}

}