package com.pcw.bcinterface.service;


import java.util.Optional;

import com.pcw.bcinterface.entity.UserDevice;
import com.pcw.bcinterface.model.DeviceInfo;

public interface UserDeviceService {

	UserDevice createUserDevice(DeviceInfo deviceInfo);

	Optional<UserDevice> findByUserId(Long id);


}
