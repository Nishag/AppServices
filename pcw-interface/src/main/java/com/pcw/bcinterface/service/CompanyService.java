package com.pcw.bcinterface.service;



import com.pcw.bcinterface.entity.Company;
import com.pcw.bcinterface.model.CompanyModel;
import com.pcw.bcinterface.model.CompanyModel;

public interface CompanyService {

	Company createCompany(CompanyModel companyModel);
	
	Company getCompany(Long id);

	void editCompany( CompanyModel companyUpdateModel);
}
