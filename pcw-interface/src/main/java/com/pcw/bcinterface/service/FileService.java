package com.pcw.bcinterface.service;


import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {

	void saveResumeFilePath(Long pcwId,MultipartFile file);

	void storeTechLevelSheet(MultipartFile file);

	byte[] downloadTechLevelSheet();

	byte[] getResume(Long pcwId) throws Exception;

}
