package com.pcw.bcinterface.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.pcw.bcinterface.entity.City;
import com.pcw.bcinterface.entity.Company;
import com.pcw.bcinterface.entity.Country;
import com.pcw.bcinterface.entity.IndustrySector;
import com.pcw.bcinterface.entity.Login;
import com.pcw.bcinterface.entity.Pcw;
import com.pcw.bcinterface.entity.PcwCertificate;
import com.pcw.bcinterface.entity.PcwEducation;
import com.pcw.bcinterface.entity.PcwProject;
import com.pcw.bcinterface.entity.PcwSocialMedia;
import com.pcw.bcinterface.entity.Role;
import com.pcw.bcinterface.entity.Tech;
import com.pcw.bcinterface.entity.TechLevel;
import com.pcw.bcinterface.entity.User;
import com.pcw.bcinterface.entity.UserTechRole;
import com.pcw.bcinterface.exception.CustomException;
import com.pcw.bcinterface.model.CertificateModel;
import com.pcw.bcinterface.model.EducationModel;
import com.pcw.bcinterface.model.PcwProfileModel;
import com.pcw.bcinterface.model.ProjectModel;
import com.pcw.bcinterface.model.SignUpModel;
import com.pcw.bcinterface.model.SocialMediaModel;
import com.pcw.bcinterface.model.TechRoleLevel;
import com.pcw.bcinterface.model.utils.StringConstants;
import com.pcw.bcinterface.repository.LoginRepository;
import com.pcw.bcinterface.repository.PcwCertificateRepository;
import com.pcw.bcinterface.repository.PcwEducationRepository;
import com.pcw.bcinterface.repository.PcwProjectRepository;
import com.pcw.bcinterface.repository.PcwRepository;
import com.pcw.bcinterface.repository.PcwSocialMediaRepository;
import com.pcw.bcinterface.repository.UserRepository;
import com.pcw.bcinterface.repository.UserTechRoleRepository;
import com.pcw.bcinterface.security.JwtTokenProvider;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	LoginRepository loginRepository;

	@Autowired
	PcwRepository pcwRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserTechRoleRepository userTechRoleRepository;

	@Autowired
	PcwEducationRepository pcwEducationRepository;

	@Autowired
	PcwSocialMediaRepository pcwSocialMediaRepository;

	@Autowired
	PcwProjectRepository pcwProjectRepository;

	@Autowired
	PcwCertificateRepository pcwCertificateRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Override
	@Transactional
	public String doSignUp(SignUpModel signUpModel) {

		if (!loginRepository.existsByUserName(signUpModel.getUserName())) {

			// Saving user
			User user = new User();
			user.setCompany(new Company(signUpModel.getCompanyId()));
			user.setEmail(signUpModel.getEmail());
			user.setIsdCode(signUpModel.getIsdCode());
			user.setName(signUpModel.getUserName());
			user.setPhone(signUpModel.getMobile());
			user = userRepository.save(user);

			// Saving login
			Login loginUser = new Login();
			loginUser.setCreatedOn(new Date());
			loginUser.setPassword(passwordEncoder.encode(signUpModel.getPassword()));
			loginUser.setUser(user);
			loginUser.setUserName(signUpModel.getUserName());
			loginUser.setRoles(signUpModel.getRoles());
			loginRepository.save(loginUser);

			// Saving pcw
			Pcw pcw = new Pcw();
			pcw.setCity(new City(signUpModel.getCityId()));
			pcw.setComments(signUpModel.getComments());
			pcw.setCountry(new Country(signUpModel.getCountryId()));
			pcw.setEndDate(signUpModel.getEndDate());
			pcw.setStartDate(signUpModel.getStartDate());
			pcw.setIndustrySector(new IndustrySector(signUpModel.getIndustrySectorId()));
			pcw.setIsFullTime(signUpModel.getIsFullTime());
			pcw.setStartDate(signUpModel.getStartDate());
			pcw.setUser(user);
			pcwRepository.save(pcw);

			// Saving User Tech Role
			List<UserTechRole> userTechRoleList = new ArrayList<>();
			for (TechRoleLevel techRoleLevel : signUpModel.getTechRoleLevels()) {
				UserTechRole userTechRole = new UserTechRole();
				userTechRole.setTechLevel(new TechLevel(techRoleLevel.getLevelId()));
				userTechRole.setRole(new Role(techRoleLevel.getRoleId()));
				userTechRole.setTech(new Tech(techRoleLevel.getTechId()));
				userTechRole.setUser(user);
				userTechRoleList.add(userTechRole);
			}
			userTechRoleRepository.saveAll(userTechRoleList);

			// return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
		} else {
			throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
		}
		return StringConstants.signupSuccess;
	}

	@Override
	public String login(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			return jwtTokenProvider.createToken(username, loginRepository.findByUserNameOrEmail(username).getRoles());
		} catch (AuthenticationException e) {
			throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public String createToken(String username) {
		return jwtTokenProvider.createToken(username, loginRepository.findByUserNameOrEmail(username).getRoles());
	}

	@Override
	public void createProfile(PcwProfileModel pcwProfileModel) {

		// Saving PcwEducation
		List<PcwEducation> pcwEducationList = new ArrayList<>();
		for (EducationModel educationModel : pcwProfileModel.getEduList()) {
			PcwEducation pcwEducation = new PcwEducation();
			pcwEducation.setDegreeName(educationModel.getDegree());
			pcwEducation.setPcw(new Pcw(pcwProfileModel.getPcwId()));
			pcwEducation.setUniversityName(educationModel.getUniversity());
			pcwEducation.setYear(pcwEducation.getYear());
			pcwEducationList.add(pcwEducation);
		}
		pcwEducationRepository.saveAll(pcwEducationList);

		// Saving PcwProject
		List<PcwProject> pcwProjectList = new ArrayList<>();
		for (ProjectModel projectModel : pcwProfileModel.getProjectList()) {
			PcwProject pcwProject = new PcwProject();
			pcwProject.setDescription(projectModel.getDescription());
			pcwProject.setPcw(new Pcw(pcwProfileModel.getPcwId()));
			pcwProject.setTechIds(projectModel.getTechId());
			pcwProject.setTitle(projectModel.getTitle());
			pcwProjectList.add(pcwProject);
		}
		pcwProjectRepository.saveAll(pcwProjectList);

		// Saving PcwCertificates
		List<PcwCertificate> pcwCertificateList = new ArrayList<>();
		for (CertificateModel certificateModel : pcwProfileModel.getCertList()) {
			PcwCertificate pcwCertificate = new PcwCertificate();
			pcwCertificate.setPcw(new Pcw(pcwProfileModel.getPcwId()));
			pcwCertificate.setTitle(certificateModel.getTitle());
			pcwCertificate.setType(certificateModel.getType());
			pcwCertificate.setYear(certificateModel.getYear());
			pcwCertificateList.add(pcwCertificate);
		}
		pcwCertificateRepository.saveAll(pcwCertificateList);

		// Saving PcwSocialMediaLinks
		List<PcwSocialMedia> pcwSocialMediaLinkList = new ArrayList<>();
		for (SocialMediaModel socialMediaModel : pcwProfileModel.getSocMedList()) {
			PcwSocialMedia pcwSocialMediaLink = new PcwSocialMedia();
			pcwSocialMediaLink.setPcw(new Pcw(pcwProfileModel.getPcwId()));
			pcwSocialMediaLink.setLink(socialMediaModel.getLink());
			pcwSocialMediaLink.setType(socialMediaModel.getType());
			pcwSocialMediaLinkList.add(pcwSocialMediaLink);
		}
		pcwSocialMediaRepository.saveAll(pcwSocialMediaLinkList);

	}

	@Override
	public String editProfile(PcwProfileModel pcwProfileModel) {

		try {

			Pcw pcw = pcwRepository.getOne(pcwProfileModel.getPcwId());

			Map<Object, List<PcwCertificate>> pcwCertificatesGroupById = pcw.getPcwCertificates().stream()
					.collect(Collectors.groupingBy(w -> w.getId()));

			Map<Object, List<PcwEducation>> pcwEducationsGroupById = pcw.getPcwEducations().stream()
					.collect(Collectors.groupingBy(w -> w.getId()));

			Map<Object, List<PcwProject>> pcwProjectsGroupById = pcw.getPcwProjects().stream()
					.collect(Collectors.groupingBy(w -> w.getId()));

			Map<Object, List<PcwSocialMedia>> pcwSocialMediasGroupById = pcw.getPcwSocialMedias().stream()
					.collect(Collectors.groupingBy(w -> w.getId()));

			// Do Add,Update Delete On PcwCertificate With Pcw
			List<PcwCertificate> pcwSaveAndUpdateCertificateList = new ArrayList<>();
			for (CertificateModel certificateModel : pcwProfileModel.getCertList()) {
				PcwCertificate pcwCertificate = new PcwCertificate();
				if (pcwCertificatesGroupById.get(certificateModel.getCertId()) != null) {
					pcwCertificate = pcwCertificatesGroupById.get(certificateModel.getCertId()).get(0);
				}
				pcwCertificate.setPcw(new Pcw(pcwProfileModel.getPcwId()));
				pcwCertificate.setTitle(certificateModel.getTitle());
				pcwCertificate.setType(certificateModel.getType());
				pcwCertificate.setYear(certificateModel.getYear());
				pcwSaveAndUpdateCertificateList.add(pcwCertificate);
			}
			pcw.getPcwCertificates().clear();
			pcw.getPcwCertificates().addAll(pcwSaveAndUpdateCertificateList);

			// Do Add,Update Delete On PcwEducation With Pcw
			List<PcwEducation> pcwSaveAndUpdateEducationList = new ArrayList<>();
			for (EducationModel educationModel : pcwProfileModel.getEduList()) {
				PcwEducation pcwEducation = new PcwEducation();
				if (pcwCertificatesGroupById.get(educationModel.getEduId()) != null) {
					pcwEducation = pcwEducationsGroupById.get(educationModel.getEduId()).get(0);
				}
				pcwEducation.setPcw(new Pcw(pcwProfileModel.getPcwId()));
				pcwEducation.setDegreeName(educationModel.getDegree());
				pcwEducation.setPcw(new Pcw(pcwProfileModel.getPcwId()));
				pcwEducation.setUniversityName(educationModel.getUniversity());
				pcwEducation.setYear(pcwEducation.getYear());
				pcwSaveAndUpdateEducationList.add(pcwEducation);
			}
			pcw.getPcwEducations().clear();
			pcw.getPcwEducations().addAll(pcwSaveAndUpdateEducationList);

			// Do Add,Update Delete On PcwProject With Pcw
			List<PcwProject> pcwSaveAndUpdateProjectList = new ArrayList<>();
			for (ProjectModel projectModel : pcwProfileModel.getProjectList()) {
				PcwProject pcwProject = new PcwProject();
				if (pcwProjectsGroupById.get(projectModel.getProjId()) != null) {
					pcwProject = pcwProjectsGroupById.get(projectModel.getProjId()).get(0);
				}
				pcwProject.setPcw(new Pcw(pcwProfileModel.getPcwId()));
				pcwProject.setDescription(projectModel.getDescription());
				pcwProject.setPcw(new Pcw(pcwProfileModel.getPcwId()));
				pcwProject.setTechIds(projectModel.getTechId());
				pcwProject.setTitle(projectModel.getTitle());
				pcwSaveAndUpdateProjectList.add(pcwProject);
			}
			pcw.getPcwProjects().clear();
			pcw.getPcwProjects().addAll(pcwSaveAndUpdateProjectList);

			// Do Add,Update Delete On PcwSocialLink With Pcw
			List<PcwSocialMedia> pcwSaveAndUpdateSocialList = new ArrayList<>();
			for (SocialMediaModel socialMediaModel : pcwProfileModel.getSocMedList()) {
				PcwSocialMedia pcwSocialMedia = new PcwSocialMedia();
				if (pcwSocialMediasGroupById.get(socialMediaModel.getSmId()) != null) {
					pcwSocialMedia = pcwSocialMediasGroupById.get(socialMediaModel.getSmId()).get(0);
				}
				pcwSocialMedia.setPcw(new Pcw(pcwProfileModel.getPcwId()));
				pcwSocialMedia.setLink(socialMediaModel.getLink());
				pcwSocialMedia.setType(socialMediaModel.getType());
				pcwSaveAndUpdateSocialList.add(pcwSocialMedia);
			}
			pcw.getPcwSocialMedias().clear();
			pcw.getPcwSocialMedias().addAll(pcwSaveAndUpdateSocialList);
			pcwRepository.save(pcw);
			return StringConstants.pcwUpdateSuccess;
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	@Override
	public PcwProfileModel getProfile(Long pcwId) {
		Pcw pcw = pcwRepository.getOne(pcwId);
		PcwProfileModel pcwProfileModel = new PcwProfileModel();
		pcwProfileModel.setPcwId(pcw.getId());
		pcwProfileModel.setIndustrySectorId(pcw.getIndustrySector().getId());
		for (PcwCertificate pcwCertificate : pcw.getPcwCertificates()) {
			CertificateModel certificateModel = new CertificateModel();
			certificateModel.setCertId(pcwCertificate.getId());
			certificateModel.setTitle(pcwCertificate.getTitle());
			certificateModel.setType(pcwCertificate.getType());
			certificateModel.setYear(pcwCertificate.getYear());
			pcwProfileModel.getCertList().add(certificateModel);
		}
		for (PcwEducation pcwEducation : pcw.getPcwEducations()) {
			EducationModel educationModel = new EducationModel();
			educationModel.setDegree(pcwEducation.getDegreeName());
			educationModel.setEduId(pcwEducation.getId());
			educationModel.setUniversity(pcwEducation.getUniversityName());
			educationModel.setYear(pcwEducation.getYear());
			pcwProfileModel.getEduList().add(educationModel);
		}

		for (PcwProject pcwProject : pcw.getPcwProjects()) {
			ProjectModel projectModel = new ProjectModel();
			projectModel.setDescription(pcwProject.getDescription());
			projectModel.setProjId(pcwProject.getId());
			projectModel.setTechId(pcwProject.getTechIds());
			projectModel.setTitle(pcwProject.getTitle());
			pcwProfileModel.getProjectList().add(projectModel);
		}

		for (PcwSocialMedia pcwSocialMedia : pcw.getPcwSocialMedias()) {
			SocialMediaModel socialMediaModel = new SocialMediaModel();
			socialMediaModel.setSmId(pcwSocialMedia.getId());
			socialMediaModel.setLink(pcwSocialMedia.getLink());
			socialMediaModel.setType(pcwSocialMedia.getLink());
			pcwProfileModel.getSocMedList().add(socialMediaModel);
		}

		return pcwProfileModel;
	}

	@Override
	public void deleteProfile(Long pcwId) {
		try {
			Pcw pcw = pcwRepository.getOne(pcwId);
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.add(Calendar.DATE, 30);
			pcw.setDeleteOn(c.getTime());
			pcwRepository.save(pcw);
		} catch (Exception e) {
		}
	}

}
