package com.pcw.bcinterface.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.pcw.bcinterface.model.HiglightErrors;

@Service
public class GeneralNeutralityServiceImpl implements GeneralNeutralityService {


	@Override
	public Map<Object, Object> isGenderNeutrality(String text, String pcwFullName) {

		String reviewedString = "";
		text.replace(".", ".#");
		text.replace(",", ",#");
		text.replace(" ", "#");
		String[] splitStr = text.split("[ #]+");
		
		List<HiglightErrors> err = new ArrayList<>();
		Map<Object, Object> responseMap = new HashMap<>();

		Map<String, String> wordsToReplace = new HashMap<>();

		wordsToReplace.put("she's", "he or she's");
		wordsToReplace.put("he's", "he or she's");
		wordsToReplace.put("he", "he or she");
		wordsToReplace.put("she", "he or she");
		wordsToReplace.put("him", "him or her");
		wordsToReplace.put("hers", "his or hers");
		wordsToReplace.put("her", "his or her");
		wordsToReplace.put("herself", "himself or herself");
		wordsToReplace.put("himself", "himself or herself");
		wordsToReplace.put("his", "his or her");
		
		List<String> words = new ArrayList<>();
		words.addAll(Arrays.asList("girl[a-z]*", "boy[a-z]*", "female[a-z]*", "male", "[a-z]*ess(es)?",
				"[a-z]*ster", "ms", "mr", "miss(es)?", "mister", "madam", "maiden", "lad", "lass(es)?", "femme",
				"feminine", "masculine", "maam", "ma'am", "sir", "man", "men", "woman", "women", "lady"));
		
		Integer index=0;

		for (int i = 0; i < splitStr.length; i++) {
			String currSTring = wordsToReplace.get(splitStr[i].toLowerCase());
			if (currSTring == null) {
				currSTring = splitStr[i];
			}
			for (String escluWord : words) {
				
				if(pcwFullName.contains(currSTring)) {
					err.add(new HiglightErrors(index, index + (currSTring.length() - 1)));
					break;
				}
				
				if (currSTring.matches(escluWord)) {
					err.add(new HiglightErrors(index, index + (currSTring.length() - 1)));
					break;
				}
			}
			index++;
			index = index + currSTring.length();
			reviewedString = reviewedString + " " + currSTring;
		}
		responseMap.put("reviewedString", reviewedString.trim());
		responseMap.put("err", err);
		return responseMap;
	}

}
