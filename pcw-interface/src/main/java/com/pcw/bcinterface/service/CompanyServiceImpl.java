package com.pcw.bcinterface.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pcw.bcinterface.entity.Company;
import com.pcw.bcinterface.exception.CustomException;
import com.pcw.bcinterface.model.CompanyModel;
import com.pcw.bcinterface.model.CompanyModel;
import com.pcw.bcinterface.repository.CompanyRepository;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	CompanyRepository companyRepo;

	@Override
	public Company createCompany(CompanyModel companyModel) {
		ObjectMapper objectMapper = new ObjectMapper();
		Company company = objectMapper.convertValue(companyModel, Company.class);
		return companyRepo.save(company);
	}

	@Override
	public Company getCompany(Long id) {
		return companyRepo.findById(id).get();
	}

	@Override
	public void editCompany(CompanyModel companyUpdateModel) {
		ObjectMapper objectMapper = new ObjectMapper();
		Optional<Company> companyOptional = companyRepo.findById(companyUpdateModel.getId());
		Company company = null;
		if (companyOptional.isPresent()) {
			company = objectMapper.convertValue(companyUpdateModel, Company.class);
		} else {
			throw new CustomException("The Company Id Not Yet Created you sent", HttpStatus.CONFLICT);
		}
		companyRepo.save(company);
	}

}
