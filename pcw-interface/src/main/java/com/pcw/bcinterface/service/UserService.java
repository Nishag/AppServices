package com.pcw.bcinterface.service;

import com.pcw.bcinterface.model.PcwProfileModel;
import com.pcw.bcinterface.model.SignUpModel;

public interface UserService {
	public String doSignUp(SignUpModel signUpModel);
	public void createProfile(PcwProfileModel pcwProfileModel);
	public String editProfile(PcwProfileModel pcwProfileModel);
	public PcwProfileModel getProfile(Long pcwId);
	public String login(String username, String password);
	String createToken(String username);
	public void deleteProfile(Long pcwId);
}
