package com.pcw.bcinterface.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcw.bcinterface.modelvalidation.customvalidator.NullOrNotBlank;

import lombok.Data;

@Data
public class PcwProfileModel {

	
	@JsonProperty("tech-role-level")
	@NotEmpty
	private List<TechRoleLevel> techRoleLevels;
	
	@JsonProperty("pcw_id")
	@NotNull
	@NotBlank
	private Long pcwId;
	
	@NotNull
	@NotBlank
	@JsonProperty("isid")
	private Long industrySectorId;
	
	@NotNull
	@JsonProperty("edu")
	private List<EducationModel> eduList = new ArrayList<>();
	
	@NotNull
	@JsonProperty("proj")
	private List<ProjectModel> projectList = new ArrayList<>();
	
	@NotNull
	@JsonProperty("cert")
	private List<CertificateModel> certList = new ArrayList<>();
	
	@NotNull
	@JsonProperty("sm")
	private List<SocialMediaModel> socMedList = new ArrayList<>();
	

}
