package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class JobSearchModel {
	
	@JsonProperty(value = "pcwid")
	private Long Pcwid;
	@JsonProperty(value = "tid")
	private Long Techid;
	@JsonProperty(value = "rid")
	private Long Roleid;
	@JsonProperty(value = "pgsz")
	private Integer Page_size;
	@JsonProperty(value = "pgn")
	private Integer Page_number;
	public JobSearchModel(Long pcwid, Long techid, Long roleid) {
		super();
		Pcwid = pcwid;
		Techid = techid;
		Roleid = roleid;
	}
	
	

}
