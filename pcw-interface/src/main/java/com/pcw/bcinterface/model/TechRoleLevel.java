package com.pcw.bcinterface.model;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TechRoleLevel {

	@JsonProperty("techid")
	private Long techId;

	@JsonProperty("roleid")
	private Long roleId;

	@JsonProperty("levelid")
	private Long levelId;

}
