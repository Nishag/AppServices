package com.pcw.bcinterface.model.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Tuple;
import javax.persistence.TupleElement;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExcelUtils {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtils.class);

	@SuppressWarnings("rawtypes")
	public byte[] writeExcelFile(List<Tuple> listValueMap, String reportName) {
		try {
			XSSFWorkbook wb = new XSSFWorkbook();
			XSSFSheet sheet = wb.createSheet(reportName);

			Integer rowIndex = 0;
			XSSFRow headerRow = sheet.createRow(rowIndex);

			// Setting Styles
			XSSFFont headerFont = setFontStyle(wb, 12, true, false);
			CellStyle headerCellStyle = setSheetCellStyle(wb, IndexedColors.GREY_25_PERCENT.getIndex(), headerFont);
			XSSFFont valueFont = setFontStyle(wb, 10, false, false);
			CellStyle valueCellStyle = setSheetCellStyle(wb, IndexedColors.WHITE.getIndex(), valueFont);

			// Setting Headers
			Integer headerColIndex = 0;
			for (TupleElement te : listValueMap.get(0).getElements()) {
				XSSFCell cell = headerRow.createCell(headerColIndex++);
				cell.setCellValue(te.getAlias());
				cell.setCellStyle(headerCellStyle);
			}

			// Setting Values
			for (int r = 0; r < listValueMap.size(); r++) {
				XSSFRow row = sheet.createRow(++rowIndex);
				Tuple valueMap = listValueMap.get(r);
				for (int c = headerRow.getFirstCellNum(); c < headerRow.getLastCellNum(); c++) {
					XSSFCell heaedrCell = headerRow.getCell(c);
					XSSFCell cell = row.createCell(c);
					cell.setCellStyle(valueCellStyle);
					if (valueMap.get(heaedrCell.getStringCellValue()) != null) {
						setCellValue(cell, valueMap.get(heaedrCell.getStringCellValue()));
					}
					// sheet.autoSizeColumn(c);
				}

			}
			ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
			wb.write(byteArrayStream);
			wb.close();
			byteArrayStream.close();
			return byteArrayStream.toByteArray();
		} catch (Exception e) {
			LOGGER.info("Exception While Generating Excel File" + e);
			return null;

		}
	}

	private void setCellValue(Cell cell, Object obj) {
		if (obj == null) {
		} else if (obj instanceof String) {
			cell.setCellValue((String) obj);
		} else if (obj instanceof Long) {
			cell.setCellValue((Long) obj);
		} else if (obj instanceof Integer) {
			cell.setCellValue((Integer) obj);
		} else if (obj instanceof Double) {
			cell.setCellValue((Double) obj);
		} else if (obj instanceof Date) {
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			cell.setCellValue(dateFormat.format(obj));
		}
	}

	public <T> byte[] writeToExcelInMultiSheets(final String sheetName, final List<T> data) {

		XSSFWorkbook workbook = null;

		try {
			Sheet sheet = null;
			workbook = new XSSFWorkbook();
			sheet = workbook.createSheet(sheetName);
			List<String> columnName = getFieldNamesForByAnnotedClass(data.get(0).getClass());
			List<String> fieldNames = getFieldNamesForClass(data.get(0).getClass());
			int rowCount = 0;
			int columnCount = 0;
			Row row = sheet.createRow(rowCount++);

			XSSFFont headerFontOne = setFontStyle(workbook, 12, true, false);
			CellStyle headerCellStyle = setSheetCellStyle(workbook, IndexedColors.GREY_25_PERCENT.getIndex(),
					headerFontOne);
			XSSFFont headerFontTwo = setFontStyle(workbook, 10, false, false);
			CellStyle headerCellStyleTwo = setSheetCellStyle(workbook, IndexedColors.WHITE.getIndex(), headerFontTwo);

			for (String fieldName : columnName) {
				Cell cell = row.createCell(columnCount++);
				cell.setCellStyle(headerCellStyle);
				cell.setCellValue(fieldName);
			}
			Class<? extends Object> classz = data.get(0).getClass();
			for (T t : data) {
				row = sheet.createRow(rowCount++);
				columnCount = 0;
				for (String fieldName : fieldNames) {
					Cell cell = row.createCell(columnCount);
					Method method = null;
					try {
						method = classz.getMethod("get" + capitalize(fieldName));
					} catch (NoSuchMethodException nme) {
						method = classz.getMethod("get" + fieldName);
					}

					Object value = method.invoke(t, (Object[]) null);
					if (value != null) {
						if (value instanceof String) {
							cell.setCellValue((String) value);
						} else if (value instanceof Long) {
							cell.setCellValue((Long) value);
						} else if (value instanceof Integer) {
							cell.setCellValue((Integer) value);
						} else if (value instanceof Double) {
							cell.setCellValue((Double) value);
						} else if (value instanceof Date) {
							DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
							cell.setCellValue(dateFormat.format(value));
						}
					}
					cell.setCellStyle(headerCellStyleTwo);
					// sheet.autoSizeColumn(columnCount);
					columnCount++;
				}
				sheet.autoSizeColumn(rowCount);
			}
			return closeWorkBook(workbook);
		} catch (Exception e) {
			LOGGER.error(" Unable to generate the log " + e);
		} finally {
			try {
				if (workbook != null) {
					workbook.close();
				}
			} catch (IOException e) {
			}
		}
		return null;
	}

	// retrieve field names from a POJO class
	private static List<String> getFieldNamesForClass(Class<?> clazz) throws Exception {
		List<String> fieldNames = new ArrayList<String>();
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (!fields[i].isAnnotationPresent(com.pcw.bcinterface.annotation.ExcludeColumn.class)) {
				fieldNames.add(fields[i].getName());
			}
		}
		return fieldNames;
	}

	private static List<String> getFieldNamesForByAnnotedClass(Class<?> clazz) throws Exception {
		List<String> fieldNames = new ArrayList<String>();
		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (!fields[i].isAnnotationPresent(com.pcw.bcinterface.annotation.ExcludeColumn.class)) {
				if (fields[i].isAnnotationPresent(com.pcw.bcinterface.annotation.Column.class)) {
					fieldNames.add(fields[i].getDeclaredAnnotation(com.pcw.bcinterface.annotation.Column.class).name());
				} else {
					fieldNames.add(fields[i].getName());
				}
			}
		}
		return fieldNames;
	}

	// capitalize the first letter of the field name for retriving value of the
	// field later
	private static String capitalize(String s) {
		if (s.length() == 0)
			return s;
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	private static XSSFFont setFontStyle(XSSFWorkbook workbook, int fontSize, boolean isBold, boolean isItalic) {
		XSSFFont font = workbook.createFont();
		font.setBold(isBold);
		font.setItalic(isItalic);
		font.setFontHeightInPoints((short) fontSize);
		font.setCharSet(XSSFFont.ANSI_CHARSET);
		return font;
	}

	private static CellStyle alignRowCell(Workbook workbook) {
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.LEFT);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		return cellStyle;
	}

	private static CellStyle setSheetCellStyle(XSSFWorkbook workbook, short bgColor, XSSFFont headerFontOne) {
		CellStyle cellStyle = alignRowCell(workbook);
		return basicCellStyles(bgColor, headerFontOne, cellStyle);
	}

	private static CellStyle basicCellStyles(short bgColor, XSSFFont headerFontOne, CellStyle cellStyle) {
		cellStyle.setFillForegroundColor(bgColor);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setFont(headerFontOne);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		return cellStyle;
	}

	private static byte[] closeWorkBook(XSSFWorkbook workbook) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workbook.write(bos);
			bos.close();
			return bos.toByteArray();
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}
}