package com.pcw.bcinterface.model.utils;

import lombok.Data;

@Data
public class StringConstants {
	
	public static String requestRejected = "RequestRejected";
	public static String requestAccepted = "RequestAccepted";
	public static String signupSuccess = "SignUp Success";
	public static String pcwUpdateSuccess = "Pcw Update Success";
	public static String suc = "suc";
	public static String err = "err";
	public static String trueStr = "true";
	public static String PENDING = "Pending";
	

}
