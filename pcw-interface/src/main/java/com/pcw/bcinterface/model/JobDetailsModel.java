package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class JobDetailsModel {

	@JsonProperty(value = "jid")
	private Long jobId;
	
	@JsonProperty(value = "tle")
	private String title;
	
	@JsonProperty(value = "rolename")
	private String roleName;
	
	@JsonProperty(value = "techname")
	private String techName;
	
	@JsonProperty(value = "company_name")
	private String companyName;
	
	@JsonProperty(value = "status")
	private String status;

	public JobDetailsModel(Long jobId, String title, String roleName, String techName, String companyName,
			String status) {
		super();
		this.jobId = jobId;
		this.title = title;
		this.roleName = roleName;
		this.techName = techName;
		this.companyName = companyName;
		this.status = status;
	}
	
	
}
