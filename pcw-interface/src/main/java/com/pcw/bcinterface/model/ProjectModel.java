package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProjectModel {

	@JsonProperty("projId")
	private Long projId;
	
	@JsonProperty("tle")
	private String title;

	@JsonProperty("tid")
	private String techId;

	@JsonProperty("desc")
	private String description;

}
