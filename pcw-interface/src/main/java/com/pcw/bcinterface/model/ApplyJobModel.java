package com.pcw.bcinterface.model;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ApplyJobModel {
	
	@JsonProperty(value = "jid")
	@Min(value=1)
	private Long jobId;
	
	@JsonProperty(value = "pcwid")
	@Min(value=1)
	private Long pcwId;

}
