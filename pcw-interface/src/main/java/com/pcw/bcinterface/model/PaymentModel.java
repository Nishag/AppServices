package com.pcw.bcinterface.model;

import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PaymentModel {

	@JsonProperty(value = "pcwid")
	@Min(value=1)
	private Long pcwId;
	
	@JsonProperty(value = "bnme")
	private String bankName;
	
	@JsonProperty(value = "ifsc")
	private String IFSCCode;
}
