package com.pcw.bcinterface.model.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.core.io.FileSystemResource;

public class TemplateAppender {
	
	public static final String TEMPLATE_PROPERTIES = "config"+File.separator+"template_message.properties";
	
	public static TemplateAppender emailTemplateAppender;
	
	
	/**
     * <h1>
     * Email Template Appender singleton.
     * </h1>
     * <p>
     * Creating singleton for email template appender.
     * </p>
     * @return - returns the email template appender.
     */
    public static synchronized TemplateAppender getInstance() {
        if (emailTemplateAppender == null) {
        	emailTemplateAppender = new TemplateAppender();
        }
        return emailTemplateAppender;
    }
    
    /**
     * <p>
     * 	Get email template given the template path
     * </p>
     * 
     * @param templatePath
     * @return email template
     */
    public String getEmailTemplate(String templatePath) {
    	InputStream inputStream = getClass().getClassLoader().getResourceAsStream(templatePath);
    	String emailTemplate =   new BufferedReader(new InputStreamReader(inputStream)).lines().collect(Collectors.joining("\n"));
		return emailTemplate;
    	
    }
    
    /**
     * <h1>
     * Get Message from TEMPLATE_MESSAGE_PROPERTIES file based on the message key.
     * </h1>
     * @param messageKey - message key is passed in this attribute.
     * @return - returns the constructed message.
     */
    public String getMessage(String messageKey) {
        final Properties configProp = new Properties();
        try {
            InputStream inputStream = new FileSystemResource(TEMPLATE_PROPERTIES).getInputStream();
            configProp.load(inputStream);
        } catch (IOException ioe) {
        	System.out.println(ioe.getMessage());
        }
        return configProp.getProperty(messageKey);
    }
    
	public static String read(String fileName) {
		Path path;
		StringBuilder data = new StringBuilder();
		Stream<String> lines = null;
		try {
			path = Paths.get(Thread.currentThread().getContextClassLoader().getResource(fileName).toURI());
			lines = Files.lines(path);
		} catch (URISyntaxException | IOException e) {
			throw new RuntimeException(e);
		}
		lines.forEach(line -> data.append(line));
		lines.close();
		return data.toString();
	}
	
	public static String parseTemplateFromParams(String htmlTemplate, Map<String, String> data) throws Exception {
		if (data != null && !data.isEmpty()) {
			String[] result = new String[1];
			result[0] = htmlTemplate;
			data.forEach((key, value) -> {
				result[0] = result[0].replace("${" + key + "}", value);
			});

			// Again replacing the values for parsing template content
			data.forEach((key, value) -> {
				result[0] = result[0].replace("${" + key + "}", value);
			});
			return result[0];
		}
		return htmlTemplate;
	}
    
}
