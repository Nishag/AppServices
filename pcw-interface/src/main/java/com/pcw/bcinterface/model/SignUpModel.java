package com.pcw.bcinterface.model;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcw.bcinterface.entity.LoginRoles;
import com.pcw.bcinterface.modelvalidation.customvalidator.EndDateValidateWithStartDate;

import lombok.Data;

@Data
@Validated
@EndDateValidateWithStartDate
public class SignUpModel {

	@Size(min = 3, max = 64,message = "Name length should between 3 to 64")
	@JsonProperty("nme")
	private String name;
	
	
	//@Pattern(regexp="((?=.*[a-z])(?=.*\\\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,32})")
	@Size(min = 8, max = 32,message = "Name length should between 8 to 32")
	@JsonProperty("pass")
	private String password;

	@Pattern(regexp="[\\w_\\.]+")
	@Size(min = 5, max = 20,message = "Name length should between 5 to 20")
	@JsonProperty("unme")
	private String userName;

	@JsonProperty("eml")
	@Email(message = "Please give valid email")
	private String email;

	@JsonProperty("catid")
	private Long categoryId;

	@JsonProperty("cid")
	private Long companyId;

	@JsonProperty("tech-role-level")
	private List<TechRoleLevel> techRoleLevels;

	@Pattern(regexp="[7-9][0-9]{9}")
	@JsonProperty("mob")
	private String mobile;

	@Pattern(regexp="(91)",message = "enter correct isd code as 91")
	@JsonProperty("isd")
	private String isdCode;

	@JsonProperty("isid")
	private Long industrySectorId;

	@JsonProperty("coid")
	private Long countryId;

	@JsonProperty("ctid")
	private Long cityId;

	@Size(min = 0, max = 500,message = "comments length should between 0 to 500")
	@JsonProperty("cmt")
	private String comments;

	@JsonProperty("sd")
	private Date startDate;

	@JsonProperty("ed")
	private Date endDate;

	@JsonProperty("ift")
	private Boolean isFullTime;
	
	@JsonProperty("login-roles")
	List<LoginRoles> roles;

}
