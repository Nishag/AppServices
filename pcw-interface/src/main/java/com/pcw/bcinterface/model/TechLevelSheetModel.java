package com.pcw.bcinterface.model;

import com.pcw.bcinterface.annotation.Column;

import lombok.Data;

@Data
public class TechLevelSheetModel {

	@Column(name = "Technology")
	private String technology;

	@Column(name = "Role")
	private String role;

	@Column(name = "Level 1")
	private Integer countLevel1 = 0;

	@Column(name = "Level 2")
	private Integer countLevel2 = 0;

	@Column(name = "Level 3")
	private Integer countLevel3 = 0;

	@Column(name = "Level 4")
	private Integer countLevel4 = 0;

	@Column(name = "Level 5")
	private Integer countLevel5 = 0;

}
