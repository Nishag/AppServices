package com.pcw.bcinterface.model;

import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.pcw.bcinterface.entity.Login;

public class MyLoginUserDetails implements UserDetails {

	private static final long serialVersionUID = 1L;
	private Login loginUser;

	Set<GrantedAuthority> authorities = null;

	public Login getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(Login loginUser) {
		this.loginUser = loginUser;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public String getPassword() {
		return loginUser.getPassword();
	}

	public String getUsername() {
		return loginUser.getUserName();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}

}