package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class SocialMediaModel {

	@JsonProperty("smId")
	private Long smId;
	
	@JsonProperty("lnk")
	private String link;

	@JsonProperty("tpe")
	private String type;


}
