package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CertificateModel {

	@JsonProperty("certId")
	private Long certId;
	
	@JsonProperty("tle")
	private String title;

	@JsonProperty("tpe")
	private String type;

	@JsonProperty("yr")
	private Integer year;

}
