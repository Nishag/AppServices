package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class JobsModel {

	@JsonProperty(value = "jid")
	private Long jobId;

	@JsonProperty(value = "tle")
	private String title;

	@JsonProperty(value = "rolename")
	private String roleName;

	@JsonProperty(value = "techname")
	private String techName;

	@JsonProperty(value = "company_name")
	private String companyName;

	public JobsModel(Long jobId, String title, String roleName, String techName, String companyName) {
		super();
		this.jobId = jobId;
		this.title = title;
		this.roleName = roleName;
		this.techName = techName;
		this.companyName = companyName;
	}

}
