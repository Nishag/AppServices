package com.pcw.bcinterface.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CompanyModel {

	@JsonProperty("id")
	private Long id;

	@Size(min = 5, max = 20, message = "Name length should between 5 to 20")
	@JsonProperty("nme")
	private String CompanyName;

	@Size(min = 5, max = 20, message = "Name length should between 5 to 20")
	@JsonProperty("snme")
	private String simpleName;

	@JsonProperty("rno")
	private Long registrationNumber;

	@JsonProperty("cown")
	private String contractOwner;

	@JsonProperty("mcid")
	private Long MContactId;

	@JsonProperty("eml")
	@Email(message = "Please give valid email")
	private String communicationEmail;

	@JsonProperty("add")
	private String communicationPhysicalAddress;

	@JsonProperty("col1")
	private String companyAddressLine1;

	@JsonProperty("col2")
	private String companyAddressLine2;

	@JsonProperty("ctid")
	private Long cityId;

	@JsonProperty("st")
	private String state;

	@JsonProperty("co")
	private Long countryId;

	@JsonProperty("pc")
	private String postalCode;

	@JsonProperty("cnme")
	private String contactName;

	@JsonProperty("cno")
	@Size(min = 5, max = 20, message = "Name length should between 5 to 20")
	private String contactNo;

	@JsonProperty("ceml")
	@Email(message = "Please give valid email")
	private String contactEmail;

	@JsonProperty("web")
	private String websiteLink;

	@JsonProperty("syn")
	@Size(max = 6000, message = "maximum allowed char is 6000")
	private String synopsis;

	@JsonProperty("img_path")
	private String companyImagePath;
}
