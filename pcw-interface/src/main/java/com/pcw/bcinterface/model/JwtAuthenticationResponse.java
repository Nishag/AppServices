package com.pcw.bcinterface.model;

import lombok.Data;

@Data
public class JwtAuthenticationResponse {

	private String accessToken;

	private String refreshToken;

	private String tokenType;

	private Long expiryDuration;

	private Long userId;

	private String userType;

	public JwtAuthenticationResponse(String accessToken, String refreshToken, Long expiryDuration, Long userId,
			String userType) {
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.expiryDuration = expiryDuration;
		this.userId = userId;
		this.userType = userType;
		tokenType = "Bearer ";
	}

}
