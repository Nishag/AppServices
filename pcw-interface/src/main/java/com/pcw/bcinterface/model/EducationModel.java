package com.pcw.bcinterface.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class EducationModel {

	@JsonProperty("eduId")
	private Long eduId;
	
	@JsonProperty("uni")
	private String university;

	@JsonProperty("deg")
	private String degree;

	@JsonProperty("yr")
	private Integer year;
	
	
}
