package com.pcw.bcinterface.model;

import java.time.Instant;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.NaturalId;

import com.pcw.bcinterface.entity.UserDevice;

import lombok.Data;

@Entity(name = "refresh_token")
@Data
public class RefreshToken {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "refresh_token_seq")
	@SequenceGenerator(name = "refresh_token_seq", allocationSize = 1)
	private Long id;

	@Column(name = "token", nullable = false, unique = true)
	@NaturalId(mutable = true)
	private String token;

	@OneToOne(optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_device_id", unique = true)
	private UserDevice userDevice;

	@Column(name = "refresh_count")
	private Long refreshCount;

	@Column(name = "expiry_date", nullable = false)
	private Instant expiryDate;

	public RefreshToken() {
	}

	public RefreshToken(Long id, String token, UserDevice userDevice, Long refreshCount, Instant expiryDate) {
		this.id = id;
		this.token = token;
		this.userDevice = userDevice;
		this.refreshCount = refreshCount;
		this.expiryDate = expiryDate;
	}

	public void incrementRefreshCount() {
		refreshCount = refreshCount + 1;
	}

}
