package com.pcw.bcinterface.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.pcw.bcinterface.modelvalidation.customvalidator.NullOrNotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "Login Request", description = "The login request payload")
@Data
public class LoginRequest {

    @NullOrNotBlank(message = "Give Login Username or email cannot blank")
    @ApiModelProperty(value = "Registered username or email ", allowableValues = "NonEmpty String", allowEmptyValue = false)
    private String usernameOrEmail;

    @NotNull(message = "Login password cannot be blank")
    @ApiModelProperty(value = "Valid user password", required = true, allowableValues = "NonEmpty String")
    private String password;

    @Valid
    @NotNull(message = "Device info cannot be null")
    @ApiModelProperty(value = "Device info", required = true, dataType = "object", allowableValues = "A valid " +
            "deviceInfo object")
    private DeviceInfo deviceInfo;

    public LoginRequest(String usernameOrEmail, String password, DeviceInfo deviceInfo) {
        this.usernameOrEmail = usernameOrEmail;
        this.password = password;
        this.deviceInfo = deviceInfo;
    }

    public LoginRequest() {
    }

  
}
