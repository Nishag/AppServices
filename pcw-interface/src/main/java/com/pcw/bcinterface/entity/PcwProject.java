package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pcw_project")
@Data
public class PcwProject extends AbstractEntity {

	@ManyToOne
	@JoinColumn(name = "pcw_id")
	private Pcw pcw;

	@Column(name = "title")
	private String title;

	@Column(name = "tech_id")
	private String techIds;

	@Column(name = "description")
	private String description;

	public PcwProject() {
		super();
	}

	public PcwProject(Long id) {
		super(id);
	}

}