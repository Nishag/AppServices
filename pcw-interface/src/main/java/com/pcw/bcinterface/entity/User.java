package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user_reg")
@Data
public class User extends AbstractEntity {

	@Column(name = "name")
	private String name;

	@Column(name = "email", unique = true)
	private String email;

	@Column(name = "phone")
	private String phone;

	@Column(name = "isd_code")
	private String isdCode;

	@OneToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public User() {
		super();
	}

	public User(Long id) {
		super(id);
	}

}