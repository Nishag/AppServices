package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "pcw_bank_details")
public class PcwBankDetails extends AbstractEntity{
	
	@ManyToOne
	@JoinColumn(name = "pcw_id")
	private Pcw pcw;

	@Column(name="bank")
	private String bankName;
	
	@Column(name = "ifsc")
	private String ifscCode;
	
	public PcwBankDetails() {
		super();
	}

	public PcwBankDetails(Long id) {
		super(id);
	}
}
