package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "country")
@Data
public class Country extends AbstractEntity {

	@Column(name = "name")
	private String name;

	public Country() {
		super();
	}

	public Country(Long id) {
		super(id);
	}

}