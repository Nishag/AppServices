package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user_tech_role")
@Data
public class UserTechRole extends AbstractEntity {

	@OneToOne
	@JoinColumn(name = "tech_level_id")
	private TechLevel techLevel;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	@OneToOne
	@JoinColumn(name = "tech_id")
	private Tech tech;

	@OneToOne
	@JoinColumn(name = "role_id")
	private Role role;

	public UserTechRole() {
		super();
	}

	public UserTechRole(Long id) {
		super(id);
	}

	

}