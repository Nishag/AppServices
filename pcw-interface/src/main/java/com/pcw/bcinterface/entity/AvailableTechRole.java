package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "available_tech_role")
@Data
public class AvailableTechRole extends AbstractEntity {
	
	@OneToOne
	@JoinColumn(name = "tech_id")
	private Tech tech;
	
	@OneToOne
	@JoinColumn(name = "role_id")
	private Role role;
	
	@OneToOne
	@JoinColumn(name = "tech_level_id")
	private TechLevel techLevel;
	
	@Column(name = "count")
	private Integer count;

	public AvailableTechRole() {
		super();
	}

	public AvailableTechRole(Long id) {
		super(id);
	}

}