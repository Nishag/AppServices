package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tech")
@Data
public class Tech extends AbstractEntity {

	@Column(name = "name", unique = true)
	private String name;

	@Column(name = "is_enabled")
	private Boolean isEnabled;

	@Column(name = "pic_id")
	private Long picid;

	public Tech() {
		super();
	}

	public Tech(Long id) {
		super(id);
	}

}