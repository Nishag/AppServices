package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pcw_social_media_links")
@Data
public class PcwSocialMedia extends AbstractEntity {

	@ManyToOne
	@JoinColumn(name = "pcw_id")
	private Pcw pcw;

	@Column(name = "link")
	private String link;

	@Column(name = "type")
	private String type;

	public PcwSocialMedia() {
		super();
	}

	public PcwSocialMedia(Long id) {
		super(id);
	}

}