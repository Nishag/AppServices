package com.pcw.bcinterface.entity;

import org.springframework.security.core.GrantedAuthority;

public enum LoginRoles implements GrantedAuthority {
	PCW, FCW, M_ADMIN, P_ADMIN, C_MANAGER;

  public String getAuthority() {
    return name();
  }

}
