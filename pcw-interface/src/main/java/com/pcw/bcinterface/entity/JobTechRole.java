package com.pcw.bcinterface.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="job_tech_role")

public class JobTechRole extends AbstractEntity{
	
	@OneToOne()
	@JoinColumn(name="job_id")
	private Job job;
	
	@OneToOne
	@JoinColumn(name="tech_id")
	private Tech tech;
	
	@OneToOne
	@JoinColumn(name="role_id")
	private Role role;
	
	public JobTechRole() {
		super();
	}

	public JobTechRole(Long id) {
		super(id);
	}
	

}
