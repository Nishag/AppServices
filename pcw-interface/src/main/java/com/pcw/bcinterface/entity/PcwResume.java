package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pcw_resume")
@Data
public class PcwResume extends AbstractEntity {

	@Column(name = "file_path")
	private String filePath;

	@ManyToOne
	@JoinColumn(name = "pcw_id")
	private Pcw pcw;

}