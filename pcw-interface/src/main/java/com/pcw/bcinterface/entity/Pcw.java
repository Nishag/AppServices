package com.pcw.bcinterface.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pcw")
@Data
public class Pcw extends AbstractEntity {

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private User user;

	@OneToOne
	@JoinColumn(name = "industry_sector_id")
	private IndustrySector industrySector;

	@OneToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@OneToOne
	@JoinColumn(name = "city_id")
	private City city;

	@Column(name = "comments")
	private String comments;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "is_full_time")
	private Boolean isFullTime;

	@Column(name = "delete_on")
	private Date deleteOn;

	@Column(name = "is_deleted", columnDefinition = "boolean default false")
	private Boolean isDeleted;

	@OneToMany(mappedBy = "pcw", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<PcwCertificate> pcwCertificates;

	@OneToMany(mappedBy = "pcw", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<PcwEducation> pcwEducations = new ArrayList<>();

	@OneToMany(mappedBy = "pcw", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<PcwProject> pcwProjects = new ArrayList<>();

	@OneToMany(mappedBy = "pcw", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<PcwSocialMedia> pcwSocialMedias = new ArrayList<>();

	@OneToMany(mappedBy = "pcw", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private List<PcwResume> resume = new ArrayList<>();

	public Pcw() {
		super();
	}

	public Pcw(Long id) {
		super(id);
	}

}