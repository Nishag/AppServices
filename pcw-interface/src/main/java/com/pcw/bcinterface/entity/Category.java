package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "category")
@Data
public class Category extends AbstractEntity {

	@Column(name = "name")
	private String name;

	public Category() {
		super();
	}

	public Category(Long id) {
		super(id);
	}

}