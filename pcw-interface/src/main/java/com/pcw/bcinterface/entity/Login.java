package com.pcw.bcinterface.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "login")
@Data
public class Login extends AbstractEntity {

	@Column(name = "user_name")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "created_on")
	private Date createdOn;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@ElementCollection(fetch = FetchType.EAGER)
	List<LoginRoles> roles;
	
	

	public Login() {
		super();
	}

	public Login(Long id) {
		super(id);
	}
}