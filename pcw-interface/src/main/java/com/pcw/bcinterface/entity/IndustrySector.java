package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "industry_sector")
@Data
public class IndustrySector extends AbstractEntity {

	@Column(name = "name")
	private String name;

	@Column(name = "is_enabled", unique = true)
	private Boolean isEnabled;

	@Column(name = "pic_id")
	private Long picId;

	public IndustrySector() {
		super();
	}

	public IndustrySector(Long id) {
		super(id);
	}

}