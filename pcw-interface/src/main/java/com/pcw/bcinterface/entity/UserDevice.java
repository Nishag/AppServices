package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.pcw.bcinterface.model.DeviceType;
import com.pcw.bcinterface.model.RefreshToken;

import lombok.Data;

@Entity(name = "user_device")
@Data
public class UserDevice {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_device_seq")
	@SequenceGenerator(name = "user_device_seq", allocationSize = 1)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Column(name = "device_type")
	@Enumerated(value = EnumType.STRING)
	private DeviceType deviceType;

	@Column(name = "notification_token")
	private String notificationToken;

	@Column(name = "device_id", nullable = false)
	private String deviceId;

	@OneToOne(optional = false, mappedBy = "userDevice")
	private RefreshToken refreshToken;

	@Column(name = "is_refresh_active")
	private Boolean isRefreshActive;

	public UserDevice() {
	}

	public UserDevice(Long id, User user, DeviceType deviceType, String notificationToken, String deviceId,
			RefreshToken refreshToken, Boolean isRefreshActive) {
		this.id = id;
		this.user = user;
		this.deviceType = deviceType;
		this.notificationToken = notificationToken;
		this.deviceId = deviceId;
		this.refreshToken = refreshToken;
		this.isRefreshActive = isRefreshActive;
	}

}
