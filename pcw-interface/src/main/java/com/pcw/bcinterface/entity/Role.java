package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "role")
@Data
public class Role extends AbstractEntity {

	@Column(name = "name")
	private String name;

	@Column(name = "is_enabled", unique = true)
	private Boolean isEnabled;

	@Column(name = "is_tech_dependent")
	private Boolean isTechDependent;

	public Role() {
		super();
	}

	public Role(Long id) {
		super(id);
	}

}