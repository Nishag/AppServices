package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "tech_level")
@Data
public class TechLevel extends AbstractEntity {

	@Column(name = "name")
	private String name;

	public TechLevel() {
		super();
	}

	public TechLevel(Long id) {
		super(id);
	}

}