package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pcw_education")
@Data
public class PcwEducation extends AbstractEntity {

	@Column(name = "university_name")
	private String universityName;
	
	@Column(name = "degree_name")
	private String degreeName;
	
	@Column(name = "year")
	private Integer year;
	
	@ManyToOne
	@JoinColumn(name = "pcw_id")
	private Pcw pcw;

	public PcwEducation() {
		super();
	}

	public PcwEducation(Long id) {
		super(id);
	}

}