package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="job")
public class Job extends AbstractEntity{
	
	@Column(name="title")
	private String title;
	
	@Column(name="description")
	private String description;
	
	@OneToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	public Job() {
		super();
	}

	public Job(Long id) {
		super(id);
	}

}
