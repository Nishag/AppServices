package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Table(name = "company")
@Data
public class Company extends AbstractEntity {

	@JsonProperty("nme")
	@Column(name = "company_name")
	private String companyName;
	
	@JsonProperty("snme")
	@Column(name = "simple_name")
	private String simpleName;
	
	@JsonProperty("rno")
	@Column(name = "registration_number")
	private Long registrationNumber;
	
	@JsonProperty("cown")
	@Column(name = "contract_owner")
	private String contractOwner;
	
	@JsonProperty("mcid")
	@Column(name = "m_contact_Id")
	private Long MContactId;
	
	@JsonProperty("eml")
	@Column(name = "communication_email")
	private String communicationEmail;
	
	@JsonProperty("add")
	@Column(name = "communication_physical_address")
	private String communicationPhysicalAddress;
	
	@JsonProperty("col1")
	@Column(name = "company_address_line1")
	private String companyAddressLine1;
		
	@JsonProperty("col2")
	@Column(name = "company_address_line2")
	private String companyAddressLine2;
	
	@JsonProperty("ctid")
	@Column(name = "city_id")
	private Long cityId;
	
	@JsonProperty("st")
	@Column(name = "state")
	private String state;
	
	@JsonProperty("co")
	@Column(name = "country_id")
	private Long countryId;
	
	@JsonProperty("pc")
	@Column(name = "postal_code")
	private String postalCode;
	
	@JsonProperty("cnme")
	@Column(name = "contact_name")
	private String contactName;	
	
	@JsonProperty("cno")
	@Column(name = "contact_no")
	private String contactNo;
	
	@JsonProperty("ceml")
	@Column(name = "contact_email")
	private String contactEmail;
	
	@JsonProperty("web")
	@Column(name = "website_link")
	private String websiteLink;
	
	@JsonProperty("syn")
	@Column(name = "synopsis")
	private String synopsis;
	
	@JsonProperty("img_path")
	@Column(name = "company_image_path")
	private String companyImagePath;
	
	public Company(){
		
	}
	public Company(Long id) {
		super(id);
	}

}