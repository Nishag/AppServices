package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "pcw_certificates")
@Data
public class PcwCertificate extends AbstractEntity {

	@ManyToOne
	@JoinColumn(name = "pcw_id")
	private Pcw pcw;

	@Column(name = "title")
	private String title;

	@Column(name = "year")
	private Integer year;

	@Column(name = "type")
	private String type;

	public PcwCertificate() {
		super();
	}

	public PcwCertificate(Long id) {
		super(id);
	}

}