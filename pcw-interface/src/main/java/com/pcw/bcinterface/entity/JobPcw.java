package com.pcw.bcinterface.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="job_pcw")
public class JobPcw extends AbstractEntity{

	@OneToOne
	@JoinColumn(name="job_id")
	private Job job;
	
	@OneToOne
	@JoinColumn(name="pcw_id")
	private Pcw pcw;
	
	@Column(name="status")
	private String status;
	 
	public JobPcw() {
		super();
	}

	public JobPcw(Long id) {
		super(id);
	}
	
}
