package com.pcw.bcinterface.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pcw.bcinterface.repository.PcwRepository;
import com.pcw.bcinterface.service.PcwSchedulerService;

@Component
public class PcwScheduledJobs {

	// set this to false to disable this job; set it it true by
	@Value("${deletePcw.scheduledJob.enabled:false}")
	private boolean deletePcwScheduledJobEnabled;

	@Autowired
	PcwRepository pcwRepository;

	@Autowired
	PcwSchedulerService pcwSchedulerService;

	@Scheduled(fixedRate = 1000) // every 30 seconds
	public void deletePcw() {
		if (!deletePcwScheduledJobEnabled) {
			return;
		} else {
			pcwSchedulerService.deletePcw();
		}
	}

}
