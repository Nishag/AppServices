package com.pcw.bcinterface.security;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pcw.bcinterface.entity.Login;
import com.pcw.bcinterface.model.MyLoginUserDetails;
import com.pcw.bcinterface.repository.LoginRepository;

@Service
public class MyLoginUserDetailsService implements UserDetailsService {

	@Autowired
	private LoginRepository loginRepository;

	@Override
  public MyLoginUserDetails  loadUserByUsername(String username) throws UsernameNotFoundException {
    final Login loginUser = loginRepository.findByUserNameOrEmail(username);

    if (loginUser == null) {
      throw new UsernameNotFoundException("User '" + username + "' not found");
    }
    
    Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
    authorities.addAll(loginUser.getRoles());
    
    MyLoginUserDetails customUserDetail=new MyLoginUserDetails();
    customUserDetail.setLoginUser(loginUser);
    customUserDetail.setAuthorities(authorities);

    return customUserDetail;
	}
   

}
