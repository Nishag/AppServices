package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.PcwResume;

public interface ResumeRepository  extends JpaRepository<PcwResume, Long>{

}
