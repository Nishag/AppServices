package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.PcwCertificate;

public interface PcwCertificateRepository extends JpaRepository<PcwCertificate, Long>{
	
}