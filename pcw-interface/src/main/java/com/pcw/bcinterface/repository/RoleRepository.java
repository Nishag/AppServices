package com.pcw.bcinterface.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.Role;

public interface RoleRepository  extends JpaRepository<Role, Long>{

	List<Role> findByName(String stringCellValue);

}
