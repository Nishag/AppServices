package com.pcw.bcinterface.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.pcw.bcinterface.entity.JobPcw;
import com.pcw.bcinterface.model.JobSearchModel;

public interface JobTechRoleRepository extends PagingAndSortingRepository<JobPcw,Long>  {

	
	@Query("SELECT NEW com.pcw.bcinterface.model.JobSearchModel("
			+ "jobPcw.pcw.id,"		
			+ "jobTechRole.tech.id,"
			+ "jobTechRole.role.id)"
			+ " FROM JobPcw jobPcw,JobTechRole jobTechRole  "
			+ "WHERE  jobPcw.pcw.id=:pcwId"+" AND jobTechRole.tech.id=:techId"+" AND jobTechRole.role.id=:roleId "
			+ "AND jobPcw.job.id=jobTechRole.job.id ")
	List<JobSearchModel> getSearchJob(Long pcwId, Long techId, Long roleId, Pageable of);

}
