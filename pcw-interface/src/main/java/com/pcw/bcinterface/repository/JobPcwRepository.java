package com.pcw.bcinterface.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.pcw.bcinterface.entity.JobPcw;
import com.pcw.bcinterface.model.JobDetailsModel;
import com.pcw.bcinterface.model.JobsModel;

@Repository
public interface JobPcwRepository  extends PagingAndSortingRepository<JobPcw,Long>  {

	@Query("SELECT NEW com.pcw.bcinterface.model.JobsModel("
			+ "jobPcw.job.id,"
			+ "jobPcw.job.title,"
			+ "jobTechRole.tech.name,"
			+ "jobTechRole.role.name,"
			+ "jobPcw.job.company.companyName)"
			+ " FROM JobPcw jobPcw,JobTechRole jobTechRole  "
			+ "WHERE  jobPcw.pcw.id=:id "
			+ "AND jobPcw.job.id=jobTechRole.job.id "
			+ "AND jobPcw.status IN (:jobStatus) ORDER BY jobPcw.job.id DESC")
	List<JobsModel> getJobList(Long id, List<String> jobStatus, Pageable of);

	@Query("SELECT NEW com.pcw.bcinterface.model.JobDetailsModel("
			+"jobPcw.job.id,"
			+"jobPcw.job.title,"
			+"jobTechRole.role.name,"
			+"jobTechRole.tech.name,"
			+"jobPcw.job.company.companyName,"
			+"jobPcw.status)"
			+"FROM JobPcw jobPcw,JobTechRole jobTechRole "
			+ "WHERE jobPcw.pcw.id=:pcwId "
			+ "AND jobPcw.job.id=jobTechRole.job.id "
			+ "AND jobPcw.job.id=:jobId")
	JobDetailsModel getJobDetailList(Long pcwId, Long jobId);

	@Query("SELECT jobPcw FROM JobPcw jobPcw Where jobPcw.job.id=:jobId AND jobPcw.pcw.id=:pcwId ")
	JobPcw getJobPcwByJobIdAndPcwId(Long jobId, Long pcwId);	


}
