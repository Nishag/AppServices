package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.PcwBankDetails;

public interface PaymentRepository  extends JpaRepository<PcwBankDetails, Long>{

}
