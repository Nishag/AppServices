package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.Pcw;

public interface PcwRepository extends JpaRepository<Pcw, Long>{
	
}