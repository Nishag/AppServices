package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.User;

public interface UserRepository extends JpaRepository<User, Long>{
}