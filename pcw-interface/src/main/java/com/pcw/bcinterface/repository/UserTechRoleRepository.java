package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.UserTechRole;

public interface UserTechRoleRepository extends JpaRepository<UserTechRole, Long>{
	
}