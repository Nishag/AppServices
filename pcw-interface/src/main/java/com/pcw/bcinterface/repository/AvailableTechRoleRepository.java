package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.AvailableTechRole;

public interface AvailableTechRoleRepository  extends JpaRepository<AvailableTechRole, Long>{
}
