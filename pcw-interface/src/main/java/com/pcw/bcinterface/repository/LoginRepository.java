package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pcw.bcinterface.entity.Login;

public interface LoginRepository extends JpaRepository<Login, Long>{

	@Query("SELECT login FROM Login login WHERE login.userName =:username OR login.user.email=:username ")
	Login findByUserNameOrEmail(String username);

	boolean existsByUserName(String userName);
	
}