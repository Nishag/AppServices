package com.pcw.bcinterface.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.Tech;

public interface TechRepository  extends JpaRepository<Tech, Long>{

	List<Tech> findByName(String stringCellValue);

}
