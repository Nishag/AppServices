package com.pcw.bcinterface.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pcw.bcinterface.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

}
