package com.pcw.bcinterface.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pcw.bcinterface.entity.Company;
import com.pcw.bcinterface.model.CompanyModel;
import com.pcw.bcinterface.model.CompanyModel;
import com.pcw.bcinterface.model.utils.StringConstants;
import com.pcw.bcinterface.service.CompanyService;

@RestController
@RequestMapping
@Validated
public class CompanyController {
	
	@Autowired
	CompanyService companyService;
	
	private static Map<String, String> responseMap = new HashMap<String, String>();

	@PostMapping(path = "/createCompany")
	public @ResponseBody ResponseEntity<Map<String, String>> createCompany(@RequestBody CompanyModel companyModel ) {
		try {
			Company companyResponse = companyService.createCompany(companyModel);
			responseMap.put(StringConstants.suc, companyResponse.toString());
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}
	
	@GetMapping(path = "/getCompany")
	public  CompanyModel getCompanyDetails( @RequestParam("cid") Long id ) {
		ObjectMapper objectMapper = new ObjectMapper();
		Company company = companyService.getCompany(id);
		return objectMapper.convertValue(company, CompanyModel.class);
	}
	
	@PostMapping(path = "/editCompany")
	public @ResponseBody ResponseEntity<Map<String, String>> editCompany(@Valid @RequestBody CompanyModel companyUpdateModel ) {
		try {
			companyService.editCompany(companyUpdateModel);
			responseMap.put(StringConstants.suc, "true");
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}
	
}
