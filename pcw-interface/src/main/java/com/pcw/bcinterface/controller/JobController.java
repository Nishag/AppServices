package com.pcw.bcinterface.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pcw.bcinterface.model.ApplyJobModel;
import com.pcw.bcinterface.model.PaymentModel;
import com.pcw.bcinterface.model.utils.StringConstants;
import com.pcw.bcinterface.service.JobService;

@RestController
@RequestMapping
@Validated
public class JobController {

	@Autowired
	JobService jobService;

	@PreAuthorize("hasRole('PCW')")
	@GetMapping(path = "/getJobsList")
	public @ResponseBody ResponseEntity<Map<String, Object>> getJobsList(
			@RequestParam(value = "pcw_id", required = true) @Min(value=1) Long id,
			@RequestParam(value = "flt", required = true) List<String> jobStatus,
			@RequestParam(value = "pgsz", required = true) Integer paginationLimit,
			@RequestParam(value = "pgn", required = true) Integer pageNumber) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseMap.put(StringConstants.suc, jobService.getJobList(id, jobStatus, paginationLimit, pageNumber));
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@GetMapping(path = "/getJobDetails")
	public @ResponseBody ResponseEntity<Map<String, Object>> getJobDetails(
			@RequestParam(value = "pcwid", required = true) @Min(value=1) Long Pcw_id,
			@RequestParam(value = "jid ", required = true) @Min(value=1) Long Job_id) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseMap.put(StringConstants.suc, jobService.getJobDetailList(Pcw_id, Job_id));
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@GetMapping(path = "/SearchJob")
	@Valid
	public @ResponseBody ResponseEntity<Map<String, Object>> getSearchJob(
			@RequestParam(value = "pcwid", required = true) @Min(value=1) Long pcwid,
			@RequestParam(value = "tid ", required = true) @Min(value=1) Long techId,
			@RequestParam(value = "rid ", required = true) @Min(value=1) Long roleId,
			@RequestParam(value = "pgsz ", required = true) Integer pageSize,
			@RequestParam(value = "pgn ", required = true) Integer pageNumber) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			responseMap.put(StringConstants.suc, jobService.getSearchJob(pcwid, techId, roleId, pageSize, pageNumber));
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@PutMapping(path = "/applyJob")
	public @ResponseBody ResponseEntity<Map<String, Object>> postApplyJob(@ModelAttribute ApplyJobModel jobModel) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			jobService.applyJob(jobModel);
			responseMap.put(StringConstants.suc, StringConstants.trueStr);
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@PutMapping(path = "/acceptJobRequest")
	public @ResponseBody ResponseEntity<Map<String, Object>> postAcceptJobRequest(
			@ModelAttribute ApplyJobModel jobModel) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			jobService.acceptJobRequest(jobModel);
			responseMap.put(StringConstants.suc, StringConstants.trueStr);
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@PutMapping(path = "/rejectJobRequest")
	public @ResponseBody ResponseEntity<Map<String, Object>> postRejectJobRequest(
			@ModelAttribute ApplyJobModel jobModel) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			jobService.rejectJobRequest(jobModel);
			responseMap.put(StringConstants.suc, StringConstants.trueStr);
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@PutMapping(path = "/addPaymentDetails")
	public @ResponseBody ResponseEntity<Map<String, Object>> postPaymentDetails(
			@ModelAttribute PaymentModel paymentModel) {
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try {
			jobService.addPaymentDetails(paymentModel);
			responseMap.put(StringConstants.suc, StringConstants.trueStr);
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

}