package com.pcw.bcinterface.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pcw.bcinterface.entity.User;
import com.pcw.bcinterface.exception.UserLoginException;
import com.pcw.bcinterface.model.HiglightErrors;
import com.pcw.bcinterface.model.JwtAuthenticationResponse;
import com.pcw.bcinterface.model.LoginRequest;
import com.pcw.bcinterface.model.MyLoginUserDetails;
import com.pcw.bcinterface.model.PcwProfileModel;
import com.pcw.bcinterface.model.RefreshToken;
import com.pcw.bcinterface.model.SignUpModel;
import com.pcw.bcinterface.model.utils.StringConstants;
import com.pcw.bcinterface.security.JwtTokenProvider;
import com.pcw.bcinterface.service.AuthService;
import com.pcw.bcinterface.service.GeneralNeutralityService;
import com.pcw.bcinterface.service.UserService;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping
@Validated
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private AuthService authService;
	
	@Autowired
	private GeneralNeutralityService generalNeutService;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	private static Map<String, String> responseMap = new HashMap<String, String>();

	private static final Logger logger = Logger.getLogger(UserController.class);

	@PostMapping(path = "/signup")
	public @ResponseBody ResponseEntity<Map<String, String>> signUp(@Valid @RequestBody SignUpModel signUpModel) {
		try {
			userService.doSignUp(signUpModel);
			responseMap.put(StringConstants.suc, "true");
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@SuppressWarnings("rawtypes")
	@PostMapping("/login")
	public ResponseEntity login(@ApiParam(value = "The LoginRequest payload") @Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authService.authenticateUser(loginRequest)
				.orElseThrow(() -> new UserLoginException("Couldn't login user [" + loginRequest + "]"));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		MyLoginUserDetails myLoginUserDetails = (MyLoginUserDetails) authentication.getPrincipal();
		User user = myLoginUserDetails.getLoginUser().getUser();
		logger.info("Logged in User returned [API]: " + myLoginUserDetails.getUsername());

		return authService.createAndPersistRefreshTokenForDevice(authentication, loginRequest,user)
				.map(RefreshToken::getToken).map(refreshToken -> {
					String jwtToken = userService.createToken(loginRequest.getUsernameOrEmail());
					return ResponseEntity.ok(new JwtAuthenticationResponse(jwtToken, refreshToken, jwtTokenProvider.getExpiryDuration(),
							user.getId(), myLoginUserDetails.getLoginUser().getRoles().get(0).name()));
				})
				.orElseThrow(() -> new UserLoginException("Couldn't create refresh token for: [" + loginRequest + "]"));

	}

	@PreAuthorize("hasRole('PCW')")
	@PostMapping(path = "/createProfile")
	public @ResponseBody ResponseEntity<Map<String, String>> createProfile(
			@RequestBody PcwProfileModel pcwProfileModel) {
		try {
			userService.createProfile(pcwProfileModel);
			responseMap.put(StringConstants.suc, "true");
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@PutMapping(path = "/editProfile")
	public @ResponseBody ResponseEntity<Map<String, String>> editProfile(@RequestBody PcwProfileModel pcwProfileModel) {
		try {
			userService.editProfile(pcwProfileModel);
			responseMap.put(StringConstants.suc, "true");
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	@PreAuthorize("hasRole('PCW')")
	@GetMapping(path = "/getProfile")
	public @ResponseBody ResponseEntity<Map<String, PcwProfileModel>> getProfile(
			@RequestParam(value = "pcwid", required = true) Long pcwId) {
		Map<String, PcwProfileModel> map = new HashMap<String, PcwProfileModel>();
		try {
			PcwProfileModel pcwProfileModel = userService.getProfile(pcwId);
			map.put(StringConstants.suc, pcwProfileModel);
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, PcwProfileModel>>(map, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasRole('PCW')")
	@PutMapping(path = "/deleteProfile")
	public @ResponseBody ResponseEntity<Map<String, String>> deleteProfile(@RequestParam(value = "pcwid", required = true) Long pcwId) {
		try {
			userService.deleteProfile(pcwId);
			responseMap.put(StringConstants.suc, "true");
		} catch (Exception e) {
			responseMap.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}
	
	@GetMapping(path = "/getGenderNeutral")
	public @ResponseBody ResponseEntity<Map<Object, Object>> getGenderNeutral(
			@RequestParam(value = "text", required = true) String text,
			@RequestParam(value = "pcwFullName", required = true) String pcwFullName) {
		Map<Object, Object> responseMapp = new HashMap<>();
		try {
			responseMapp = generalNeutService.isGenderNeutrality(text, pcwFullName);
		} catch (Exception e) {
			responseMapp.put(StringConstants.err, e.getMessage());
		}
		return new ResponseEntity<Map<Object, Object>>(responseMapp, HttpStatus.OK);
	}

}