package com.pcw.bcinterface.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pcw.bcinterface.service.FileService;



@RestController
@RequestMapping
public class FileController {

	@Autowired
	FileService fileService;
	
	//@PreAuthorize("hasRole('PCW')")
	@PostMapping("/uploadResume")
	public String upload(@RequestParam("files") MultipartFile file,
			@RequestParam(value = "pcwId", required = true) Long pcwId) {
		fileService.saveResumeFilePath(pcwId, file);
		return "success";
	}
	
	//@PreAuthorize("hasRole('PCW')")
	@PostMapping("/storeTechLevelSheet")
	public String storeTechLevelSheet(@RequestParam("files") MultipartFile file) {
		fileService.storeTechLevelSheet(file);
		return "success";
	}
	
	//@PreAuthorize("hasRole('PCW')")
	@PostMapping("/downloadTechLevelSheet")
	public ResponseEntity<byte[]> downloadTechLevelSheet() {
		byte[] outStream = fileService.downloadTechLevelSheet();
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "TechLevelSheet.xlsx")
				.contentType(new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
				.contentLength(outStream.length).body(outStream);
	}
	
	// @PreAuthorize("hasRole('PCW')")
	@GetMapping("/getResume")
	public ResponseEntity<byte[]> getResume(@RequestParam(value = "pcwId", required = true) Long pcwId) throws Exception {
		byte[] pdfBuffer = null;
		try {
			pdfBuffer = fileService.getResume(pcwId);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "resume.pdf")
				.contentType(MediaType.parseMediaType("application/pdf")).contentLength(pdfBuffer.length)
				.body(pdfBuffer);
	}

}