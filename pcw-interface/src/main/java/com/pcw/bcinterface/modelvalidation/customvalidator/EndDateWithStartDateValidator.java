package com.pcw.bcinterface.modelvalidation.customvalidator;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.pcw.bcinterface.model.SignUpModel;

public class EndDateWithStartDateValidator implements ConstraintValidator<EndDateValidateWithStartDate, SignUpModel> {
    public void initialize(EndDateValidateWithStartDate constraintAnnotation) {
    }

	@Override
	public boolean isValid(SignUpModel object, ConstraintValidatorContext context) {
		 if (!(object instanceof SignUpModel)) {
	            throw new IllegalArgumentException("@EndDateValidateWithStartDate only applies to SignUpModel");
	        }
		 SignUpModel signUpModel = (SignUpModel) object;
		 Calendar c = Calendar.getInstance();
		 c.setTime(signUpModel.getStartDate());
		 c.add(Calendar.YEAR, 1);
		 Date afterOneYearFromStartDate = c.getTime();
		 return signUpModel.getEndDate().after(signUpModel.getStartDate()) && signUpModel.getEndDate().before(afterOneYearFromStartDate);
	}
}
