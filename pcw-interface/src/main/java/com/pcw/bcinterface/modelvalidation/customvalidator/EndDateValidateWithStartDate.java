package com.pcw.bcinterface.modelvalidation.customvalidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = { EndDateWithStartDateValidator.class })
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EndDateValidateWithStartDate {

	String message() default "End Date Should be Between StartDate and StartDate Plus An Year";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
